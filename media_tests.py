from django.test import TestCase
from app.media_library_app.models import UserMedia
# Create your tests here.
class UserMediaLibraryTestCase(TestCase):
    def setUp(self):
        UserMedia.objects.create(url='https://storage.googleapis.com/deft-crawler-138109.appspot.com/IMAGE/R.jpg',
                                        name='Doraemon',
                                        thumb_url='https://www.youtube.com/results?search_query=djnago')

    def test_get_method_positiveflow(self):
        url = 'http://127.0.0.1:8000/ecom/api/files/1'
        response  = self.client.get(url)
        self.assertEqual(response.status_code,200)
        qs = UserMedia.objects.filter(name = 'Doraemon')
    
        
    def test_post_method_positive(self):
        url = 'http://127.0.0.1:8000/ecom/api/files/upload'
        data = {
            'url'       : "https://storage.googleapis.com/deft-crawler-138109.appspot.com/IMAGE/R.jpg",
            'name'      : "terralogic",
            'thumb_url' : "https://www.youtube.com/results?search_query=djnago"
        }
        response = self.client.post(url,data,format = 'json')
        self.assertEqual(response.status_code,200)
        print("POST method status code:",response.status_code)

    def test_post_method_negative(self):
        url = 'http://127.0.0.1:8000/ecom/api/files/upload'
        data = {
            
            'name'      : "terralogic",
            'thumb_url' : "https://www.youtube.com/results?search_query=djnago"
        }
        response = self.client.post(url,data,format = 'json')
        self.assertEqual(response.status_code,404)
        print("POST method status code:",response.status_code)

    def test_delete_method_positive_flow(self):
        url = 'http://127.0.0.1:8000/ecom/api/files/1'
        response = self.client.delete(url)
        print(response.status_code)
        self.assertEqual(response.status_code,200)

    def test_delete_method_negative_flow(self):
        url = 'http://127.0.0.1:8000/ecom/api/files/1'
        response = self.client.delete(url)
        self.assertEqual(response.status_code,400)
        print("DELETE method status code:",response.status_code)