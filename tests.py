from http import client
from pydoc import cli
from django.test import TestCase
from django.urls import resolve,reverse
# Create your tests here.
from app.collection_app.models import Collection
from app.collection_app.views import *
from app.collection_app.urls import *
# Create your tests here.
from django.test import TestCase,Client
from django.urls import reverse
import json
class collectionTestCaseurls(TestCase):
    def setUp(self):
        Collection.objects.create(name='Terralogic collections')
    

    
    def test1(self):
        url = reverse('MyCollections')
        self.assertEquals(resolve(url).func,MyCollections)
    def test2(self):
        url = reverse('create_collection')
        self.assertEquals(resolve(url).func,Collections)
    def test3(self):
        url = reverse('CRUDCollections',args=['collection'])
        self.assertEquals(resolve(url).func,CRUDCollections)
    def test4(self):
        url = reverse('tag_collection',args=['collection'])
        self.assertEquals(resolve(url).func,TagCollection)
    def test5(self):
        url = reverse('GetCollectionsByUserName',args=['srinivas'])
        self.assertEquals(resolve(url).func,GetCollectionsByUserName)
    def test6(self):
        url = reverse('GetCollectionWithFilters',args=['filters'])
        self.assertEquals(resolve(url).func,GetCollectionWithFilters)
    